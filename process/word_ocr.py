import re

import cv2
import imutils
import numpy as np
import pytesseract
from absl import app
from matplotlib import pyplot as plt

# get grayscale image
def get_grayscale(image):
    return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


# noise removal
def remove_noise(image):
    return cv2.medianBlur(image, 5)


# thresholding
def thresholding(image):
    return cv2.threshold(image, 100, 255, cv2.THRESH_BINARY+ cv2.THRESH_OTSU)[1]
    # return cv2.threshold(image, 10, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    # return cv2.adaptiveThreshold(image,25,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)

# dilation
def dilate(image):
    kernel = np.ones((5, 5), np.uint8)
    return cv2.dilate(image, kernel, iterations=1)


# erosion
def erode(image):
    kernel = np.ones((5, 5), np.uint8)
    return cv2.erode(image, kernel, iterations=1)


# opening - erosion followed by dilation
def opening(image):
    kernel = np.ones((5, 5), np.uint8)
    return cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)

# skew correction
def deskew(image):
    coords = np.column_stack(np.where(image > 0))
    angle = cv2.minAreaRect(coords)[-1]
    if angle < -45:
        angle = -(90 + angle)
    else:
        angle = -angle
    (h, w) = image.shape[:2]
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, angle, 1.0)
    rotated = cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
    return rotated


# template matching
def match_template(image, template):
    return cv2.matchTemplate(image, template, cv2.TM_CCOEFF_NORMED)

# canny edge detection
def canny(image):
    return cv2.Canny(image, 50 , 200)

def size_threshold(bw, minimum, maximum):
    retval, labels, stats, centroids = cv2.connectedComponentsWithStats(bw)
    for val in np.where((stats[:, 4] < minimum) + (stats[:, 4] > maximum))[0]:
        labels[labels == val] = 0
    return (labels > 0).astype(np.uint8) * 255

def y_centroid_threshold(bw, minimum, maximum):
    retval, labels, stats, centroids = cv2.connectedComponentsWithStats(bw)
    for val in np.where((centroids[:, 1] < minimum) + (centroids[:, 1] > maximum))[0]:
        labels[labels == val] = 0
    return (labels > 0).astype(np.uint8) * 255

def re_practice(img, coords):
    xmin, ymin, xmax, ymax = coords
    # get the subimage that makes up the bounded region and take an additional 5 pixels on each side
    # box = img[int(ymin)-10:int(ymax)+10, int(xmin)-10:int(xmax)+10]
    size = 5
    box = img[int(ymin) - size:int(ymax) + size, int(xmin) - size:int(xmax) + size]

    # Preprocess image
    # resize image to three times as large as original for better readability
    # max_resize = 3.5
    # box = cv2.resize(box, None, fx=max_resize, fy=max_resize, interpolation=cv2.INTER_CUBIC)

    img = box
    print(img.shape)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    thres = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 31, 10)

    cv2.imshow('Thres', thres)
    # cv2.waitKey()

    contours = cv2.findContours(thres, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    contours = imutils.grab_contours(contours)
    contours = sorted(contours, key=cv2.contourArea, reverse=False)

    # loop over our contours
    number = 0
    for c in contours:
        (x, y, w, h) = cv2.boundingRect(c)
        print(x, y, w, h)
        # cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
        # approximate the contour
        if (40 < w < 90) and (100 < h < 180) and (h / w > 1.5):
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
            number += 1
            crop = img[y:y + h, x:x + w]
            cv2.imwrite("plate_number{}.png".format(number), crop)

    print("Number of Contours found = " + str(number))
    # cv2.drawContours(img, contours, -1, (0, 255, 0), 3)

    cv2.imshow('Pix', img)
    cv2.imshow('Thres', thres)
    # cv2.waitKey()

    return str(number)

def orc_image(img, coords):
    # separate coordinates from box
    xmin, ymin, xmax, ymax = coords
    # get the subimage that makes up the bounded region and take an additional 5 pixels on each side
    # box = img[int(ymin)-10:int(ymax)+10, int(xmin)-10:int(xmax)+10]
    size = 10
    box = img[int(ymin) - size:int(ymax) + size, int(xmin) - size:int(xmax) + size]

    # Preprocess image
    # resize image to three times as large as original for better readability
    max_resize = 3.5
    box = cv2.resize(box, None, fx= max_resize, fy= max_resize, interpolation=cv2.INTER_CUBIC)

    # imgray = cv2.cvtColor(box, cv2.COLOR_BGR2GRAY)
    #
    # ret, thresh = cv2.threshold(imgray, 127, 255, 0)
    #
    # sized = size_threshold(thresh, 60, 300)
    # plt.imshow(sized)
    # centered = y_centroid_threshold(sized, 40, 63)

    plt.imshow(box)
    plt.title('PICTURE ORIGINAL IMAGE')
    plt.show()

    gray = get_grayscale(box)
    # cv2.imshow("anh gray", gray)
    # cv2.waitKey()
    thresh = thresholding(gray)
    # cv2.imshow("anh thresh", thresh)
    # cv2.waitKey()
    # openingr = opening(thresh)
    # cv2.imshow("anh opening", openingr)
    # cv2.waitKey()
    # cannyr = canny(thresh)
    # cv2.imshow("anh canny", cannyr)
    # cv2.waitKey()

    # image = thresh
    # custom_config = r'--oem 3 --psm 6'
    custom_config = r'--oem 3 --psm 6'
    text = pytesseract.image_to_string(thresh, config=custom_config)
    # clean tesseract text by removing any unwanted blank spaces
    text = re.sub('[\W_]+', '', text)

    # print("text = ", text)
    return text

def main(_argv):
    link = "./data/images/c5.png"
    image = cv2.imread(link)
    b, g, r = cv2.split(image)
    rgb_img = cv2.merge([r, g, b])
    plt.imshow(rgb_img)
    plt.title('PICTURE ORIGINAL IMAGE')
    plt.show()

    print("-----")
    print("result = ", orc_image(image))

if __name__ == '__main__':
    try:
        app.run(main)
    except SystemExit:
        pass
